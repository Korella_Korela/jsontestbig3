"""
    Author: Prokofyev Daniil 
    HeadHunter: https://hh.ru/resume/0420504eff054cf9080039ed1f464c73727559
    BitBucket: https://bitbucket.org/Korella_Korela/
    Task:
        1. Create json's (test cause)
        [
            similar,
            different 1,
            different 2,
            different 3
        ]
        2. Load two json files into two dicts
        3. Find the differences
        4. Print the differences
"""
from os import path #: find the script dir's abspath
from json import load #: json loading
from deepdiff import DeepDiff #: l've checked [json_tools, jsondiff, dictdiff],
# but deepdiff is way more closer to my task
from pprint import pprint #: nice indents to print cringe-looking dicts

class JsonComparer(object):
    """
    Load json into dict
    Comparing it
    Print result
    """
    def __init__(self):
        super(JsonComparer, self).__init__()

        
    def json_to_dict(self, file):
        """
        Seqv json data to dict
        """
        here = path.abspath(path.dirname(__file__))
        with open(path.join(here, file), "r") as rf:
            jn = load(rf)
        return jn

    def compare(self, file1, file2):
        print("\n\nCOMPARING FILES:", file1, "AND", file2)
        json1 = self.json_to_dict(file1) 
        json2 = self.json_to_dict(file2)
        equal = json1==json2
        if not equal:
            print("\tThese two json files are distinguishable c(*- *)э")

            if not json1.keys() == json2.keys():
                print("\nWe've diffrent KEYS here:")
                diff1 = [ k for k in json2.keys() if k not in json1.keys() ]
                diff2 = [ k for k in json1.keys() if k not in json2.keys() ]
                print("If we look from json1 to json2:")
                pprint(DeepDiff(diff1,diff2), indent = 2)
                print("If we look from json2 to json1:")
                pprint(DeepDiff(diff2,diff1), indent = 2)

            if not json1.values() == json2.values():
                print("\nWe've diffrent VALUES here:")
                diff1 = [ k for k in json1.values() if k not in json2.values() ]
                diff2 = [ k for k in json2.values() if k not in json1.values() ]
                print("If we look from json1 to json2:")
                pprint(DeepDiff(diff1,diff2), indent = 2)
                print("If we look from json2 to json1:")
                pprint(DeepDiff(diff2,diff1), indent = 2)

        else:
            print("\tThese two json files are similar c(* -*)э")
        
def main():
    """
    head of the prog
    """
    files = ['rf1.json', 'rf2.json', 'rf3.json', 'rf4.json'] #: test data
    jsoncomp = JsonComparer()
    for file in files:
        jsoncomp.compare(files[0], file)
        
if __name__ == "__main__":
    main()
